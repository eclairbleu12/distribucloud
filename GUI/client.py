import random
import hashlib
import requests
import time, json
import os
from urllib.parse import parse_qsl
import glob 
history = {}
if os.path.isfile('history.json'):

    with open('history.json') as json_file:  
        history = json.load(json_file)
else:
    with open('history.json', 'w') as outfile:  
        json.dump(history, outfile)





piece_index = 0



def write_data(data):
    with open('history.json', 'w') as outfile:  
        json.dump(data, outfile)


def last_4chars(x):
    return(x[-4:])


def create_piece(filename,hash_gen=None):
    global piece_index
    if hash_gen:
        pass
    else:
        hash_gen = filename.split('/')[0]
  

    hash = hashlib.md5(open(filename, 'rb').read()).hexdigest()
    hash = str(piece_index) +'-'+ hash
    piece_index += 1

    files = {'file': (filename, open(filename, 'rb'))}
    print('HAsh : ',hash)
    r = requests.post('http://127.0.0.1:5000/postpiece?hash='+hash+'&filehash='+hash_gen,files=files)
    print('File sended !')

def send_file(file,mdp):
    global piece_index
    mb = 3
    hash = hashlib.md5(open(file, 'rb').read()).hexdigest()
    os.system('mkdir '+hash) 
    os.system('gpg --batch -c -a --passphrase '+mdp+' '+file)
    os.system('rm -rf '+hash+'/parts/')
    os.system('mkdir -p '+hash+'/parts')
    os.system('split -C '+str(int(float(mb)*1000000.0))+' -d '+file+'.asc '+hash+'/parts/part.')
    


    print('Hash : '+hash)
    file_list = os.listdir(hash+'/parts/')
    for filepath in sorted(file_list, key = last_4chars):
        filepath = hash+'/parts/'+filepath
        print(filepath)
        create_piece(filepath)
    piece_index = 0
    os.system('rm -rf '+''+' '+file+'.asc'+' '+hash+'/')




def listen():
    json_file = open('history.json','w')
    os.system('mkdir downloaded')
    global piece_index  
    global history
    while True:
        r = (requests.get('http://127.0.0.1:5000/do'))
        
        if 'POST' in r.text:
                        text = r.text.split('POST')[-1]
                        print('TEXT : '+text)
                        for filepath in glob.iglob('downloaded/'+text+'.part'):
                            
                            for file in history.keys():
                                if text in history[file]:
                                    file_h = file
                            

                            create_piece(filepath,file_h)
                            piece_index = 0
            
                            print('J\'ai : '+filepath)

        if 'http' in r.text:
            

            parse = parse_qsl(r.text)
            file_hash = parse[1][1]

            l = requests.get(r.text)
            if 'filename' in l.headers.keys():
                if file_hash in history.keys():
                    history[file_hash].append(parse[0][1])
                else:
                    history[file_hash] = [parse[0][1]]
                write_data(history)
                print('Va chercher la piece : '+r.text)
                open("downloaded/"+l.headers['filename'],'w').write(l.text)
                print('New piece downloaded')
            else:
                print(l.text)

        else:
            print(r.text)
        time.sleep(0.5)
        



