#! /usr/bin/python

# -*- coding:utf-8 -*-


from flask import Flask, render_template, request, redirect, url_for, flash, make_response
import client, os
import files_type
from flask import send_file

import requests
import subprocess
from datetime import *
import hashlib
from werkzeug import secure_filename
app = Flask(__name__)
app.config['SECRET_KEY'] = 'e78255661c1c4081953855f4d8119116'

files_clouded = {}


font_awesome = {
    'audio': 'fas fa-music',
    'compressed': 'fas fa-file-archive',
    'iso': 'fas fa-compact-disc',
    'db' : 'fas fa-database',
    'exec' : 'fas fa-file-code',
    'image': 'fas fa-file-image',
    'code': 'fas fa-file-code',
    'pres': 'fas fa-file-powerpoint',
    'spread': 'fas fa-table',
    'other': 'fas fa-file',
    'video': 'fas fa-file-video',
    'text': 'fas fa-file-alt',
}




DOSSIER_UPS = './uploads/'

users = {"test":{
    "password": "test",
    "mail": "email@internet.org",
    "avatar": "photo",
    "ppf" : 10,
    "dupli": 1,

}}

def multi_key_dict_get(d, k):
    for keys, v in d.items():
        if k in keys:
            return v
    return 'other'
    

def get_file_name(hash):
    for file in files_clouded.keys():
        if files_clouded[file]['hash'] == hash:
            return file
    return None


def file_type(filename):
    ext = filename.split('.')[-1]
    return multi_key_dict_get(files_type.files_type,ext)




@app.errorhandler(404)
def ma_page_404(error):
    return render_template('404.html'), 404


def is_connected(request):
    if not request.cookies.get('user'):
    
        return False
    else:
        return True


def create_user(pseudo,password,mail,ppf,dupli):
    users[pseudo] = {'password':password,"mail":mail,"avatar":"photo","ppf":ppf,"dupli":dupli}



@app.route('/prepare', methods=['GET'])
def prepare():
    hash = request.args.get('hash')
    ext = request.args.get('ext')

    file =  requests.get('http://127.0.01:5000/getfile?hash='+hash)
    if 'PGP' in file.text:
        u = open(hash+'.asc','w')
        u.write(file.text)
        u.close()
        cmd = ['gpg', '--batch','--passphrase','biscuit','-d',hash+'.asc']
        with open(hash+'.'+ext, 'w') as out:
            return_code = subprocess.call(cmd, stdout=out)
        os.remove(hash+'.asc')
    else:
        return 'Fichier inexistant'
    return 'http://127.0.0.1:5555/getfile?hash='+hash+'&ext='+ext



@app.route('/getfile', methods=['GET'])
def getfile():
    hash = request.args.get('hash')
    ext = request.args.get('ext')
    hash = hash+'.'+ext
    print(hash)
    if os.path.isfile(hash):
        name = get_file_name(hash)
        result = send_file(hash,as_attachment=True)
        return result
        
    else:
        return 'Une erreur est survenue !'
    

@app.route('/sendfile', methods=['POST'])
def sendfile():
    f = request.files['file']
    if f: # on vérifie qu'un fichier a bien été envoyé
         # on vérifie que son extension est valide
        nom = secure_filename(f.filename)
        print(nom)
        
        f.save(DOSSIER_UPS + nom)
        client.send_file(DOSSIER_UPS + nom,'biscuit')

        hash = hashlib.md5(open(DOSSIER_UPS+nom, 'rb').read()).hexdigest()


        """
{
    'FILENAME':{
        'hash':HASH,
        'type':TYPE,
        'date':DATE
    }
}




"""     
        files_clouded[nom] = {'hash':hash,'type':file_type(nom),'date':str(date.today()),'icon':font_awesome[file_type(nom)],'ext':nom.split('.')[-1]}
        print(files_clouded)

        
        

        return 'OK'

@app.route('/', methods=['GET','POST'])

def login():
    if request.method == "POST":
        pseudo = request.form['pseudo']
        password = request.form['pass']
        if pseudo in users.keys():
            if password == users[pseudo]["password"]:
                res = make_response(redirect('/cloud'))
                res.set_cookie('user', 'test', max_age=60*60*24*365*2)
                return res
        flash(u'Identifiants Invalides !')
    if not is_connected(request):
    
        return render_template('login.html')
    else:
        return redirect('/cloud')



@app.route('/cloud')

def cloud():
    if is_connected(request):
        
        return render_template('cloud.html', files=files_clouded)

    else:
        return redirect('/')
    

@app.route('/signup', methods=['GET','POST'])
def signup():
    if is_connected(request):
        return redirect('/')
    if request.method == "POST":
        try:
            pseudo = request.form['pseudo']
            email = request.form['email']
            ppf = request.form['ppf']
            dupli = request.form['dupli']
            password = request.form['password']
            create_user(pseudo,password,email,ppf,dupli)
            return redirect('/')
            
        except:
            flash(u'Vous avez mal remplis les champs')
    return render_template('signup.html', titre="Bienvenue !")



if __name__ == '__main__':

    app.run(debug=True,host="0.0.0.0",port=5555)