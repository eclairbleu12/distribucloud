files_type = {}


files_type[(
    'aif',
    'cda',
    'mid',
    'mp3',
    'mpa',
    'ogg',
    'wav',
    'wma',
    'wpl',
    'flac'
)] = "audio"


files_type[(
    '7z',
    'arj',
    'deb',
    'pkg',
    'rar',
    'rpm',
    'tar.gz',
    'z',
    'zip'

)] = "compressed"

files_type[(
    'dmg',
    'iso',
    'toast',
    'vcd'

)] = "iso"

files_type[(
    'csv',
    'dat',
    'db',
    'dbf',
    'log',
    'mdb',
    'sav',
    'sql',
    'tar',
    'xml',
    'sqlite3'

)] = 'db'


files_type[(
    'apk',
    'bat',
    'bin',
    'cgi',
    'pl',
    'com',
    'exe',
    'gadget',
    'wsf',
    'sh'

)] = "exec"

files_type[(
    'fnt','fon','otf','ttf'

)] = 'font'


files_type[(
    'ai',
    'bmp',
    'gif',
    'ico',
    'jpeg',
    'jpg',
    'png',
    'ps',
    'psd',
    'svg',
    'tif',
    'tiff'

)] = "image"

files_type[(
    'py',
    'jar',
    'asp',
    'aspx',
    'cer',
    'cfm',
    'cgi',
    'pl',
    'css',
    'html','htm',
    'js',
    'jsp',
    'part',
    'php',
    'rss',
    'xhtml',
    'c',
    'class',
    'cpp',
    'cs',
    'h',
    'java',
    'sh',
    'swift',
    'vb',
)] = "code"

files_type[(
    'odp',
    'key',
    'pps',
    'ppt',
    'pptx'

)] = "pres"

files_type[(
    'ods',
    'xlr',
    'xls',
    'xlsx'
)] = "spread"

files_type[(
    'bak',
    'cab',
    'cfg',
    'cpi',
    'cur',
    'dll',
    'dmp',
    'drv',
    'icns',
    'ico',
    'ini',
    'ink',
    'msi',
    'sys',
    'tmp'
)] = "other"

files_type[(
    '3g2',
    '3gp',
    'avi',
    'flv',
    'h264',
    'm4v',
    'mkv',
    'mov',
    'mp4',
    'mpg','mpeg',
    'rm',
    'swf',
    'vob',
    'wmv'
)] = "video"

files_type[(
    'doc', 'docx',
    'odt',
    'pdf',
    'rtf',
    'tex',
    'txt',
    'wks', 'wps',
    'wpd'
)] = 'text'