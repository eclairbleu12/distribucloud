import os

import argparse

parser = argparse.ArgumentParser(description='Encrypt and divide some files')
parser.add_argument('file', nargs='+',
                    help='Didived file')

parser.add_argument('mb', nargs='+',
                    help='Max de MB par parties')


args = parser.parse_args()
file = (args.file[0])

os.system('gpg -c -a '+file)
os.system('rm -rf parts')
os.system('mkdir parts')
os.system('split -C '+str(int(float(args.mb[0])*1000000.0))+' -d '+file+'.asc parts/part.')
os.system('rm -rf '+file+' '+file+'.asc')

