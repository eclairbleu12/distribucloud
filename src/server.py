from flask import Flask, request, redirect, render_template
from flask import send_file
from werkzeug.utils import secure_filename
import pickle
import hashlib
import os
import random
import time
os.system('mkdir tmp_parts')
app = Flask(__name__)
from flask_cors import CORS
#r = requests.get('http://127.0.01:5000/getfile?hash=cc366c7e1d77e439f926bfdfa037869e')

app.config["DEBUG"] = True
CORS(app)

ALLOWED_EXTENSIONS = set(['part', 'png', 'jpg'])


wanted = {}

liste_pieces = []

track = {}

file_register = {}

UPLOAD_FOLDER = "./tmp_parts"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


def allowed_file(filename):
    # return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    return True


@app.route('/do')
def do():
    ip = request.remote_addr
    if ip in wanted.keys():
            return 'POST'+random.choice(wanted[ip])
    else:
        pass
    if len(liste_pieces) == 0:
        

        
        return 'Don\'t do anything'
    else:
        piece = random.choice(liste_pieces)
        

        if ip in track.keys():

            if not piece.split('hash=')[1] in track[ip]:

                pass
            else:
                return 'Don\'t do anything'
        return piece



@app.route('/getfile', methods=['GET'])
def get_file():
    ip = request.remote_addr
    file_hash = request.args.get('hash')
    if file_hash in file_register.keys():
        pieces_t = file_register[file_hash].copy()
        pieces = []
        for key in pieces_t.keys():
            print('ORDER : '+key+' '+pieces_t[key])
            pieces.append(pieces_t[key])
        pieces_back = pieces.copy()
        for piece in pieces:

            if os.path.isfile('tmp_parts/'+file_hash+'/'+piece+'.part'):
                pass #Server a déja la piece
            else:

                for piece_u in track[ip]:
                    if piece in piece_u:
                        if ip in wanted.keys():
                            wanted[ip].append(piece)
                        else:
                            wanted[ip] = [piece]

        nbr = 0
        nbr_piece = len(file_register[file_hash])
        while nbr != nbr_piece:
            
            
            for piece in pieces:
                if os.path.isfile('tmp_parts/'+file_hash+'/'+piece+'.part'):
                    nbr += 1
                    pieces.remove(piece)
                
                else:
                    pass
            time.sleep(0.5)
                    

        file = []
        for piece in pieces_back:
            with open('tmp_parts/'+file_hash+'/'+piece+'.part', 'r') as content_file:
                content = content_file.read()
                file.append(content)

        f_tmp = open('tmp.txt','w')
        f_tmp.write(''.join(file))

        


        result = send_file('tmp.txt',attachment_filename='file')
        result.headers["filename"] = 'file'
        os.remove('tmp.txt')
        return result

    else:
        return 'Fichier inexistant'


@app.route('/postpiece', methods=['POST'])
def create():
    if request.method == 'POST':
        ip = request.remote_addr
        # check if the post request has the file part
        if 'file' not in request.files:
           
            return 'No file part'

        file = request.files['file']

        if file.filename == '':
            
            return 'No file selected for uploading'

        if file and allowed_file(file.filename):
            print('Enregistré')
            hash = request.args.get('hash')
            hash = hash.split('-')
            nbr = hash[0]
            hash = hash[1]
            genhash = request.args.get('filehash')
            if genhash in file_register.keys():
                file_register[genhash][nbr] = hash
            else:
                file_register[genhash] = {nbr: hash}

            print(file_register)
            filename = hash + '.part'
            if ip in wanted.keys():
                if hash in wanted[ip]:
                    wanted[ip].remove(hash)
                    if len(wanted[ip]) == 0:
                        del wanted[ip]

                if ip in track.keys():
                
                    if not hash in track[ip]:

                        track[ip].append(hash)
                else:
                    track[ip] = [hash]
            os.system('mkdir '+app.config['UPLOAD_FOLDER']+'/'+genhash)
            file.save(os.path.join(app.config['UPLOAD_FOLDER']+'/'+genhash, filename))
            liste_pieces.append('http://127.0.0.1:5000/getpiece?hash=' + hash +'&filehash='+genhash)
            return 'File(s) successfully uploaded'






@app.route('/getpiece', methods=['GET'])
def get_piece():
    ip = request.remote_addr
    hash_f = request.args.get('hash')
    genhash = request.args.get('filehash')

    if ip in track.keys():

        if not hash_f in track[ip]:

            track[ip].append(hash_f)
        else:
            return 'Vous possédez déja la piece !'
    else:
        track[ip] = [hash_f]
    liste_pieces.remove('http://127.0.0.1:5000/getpiece?hash=' + hash_f +'&filehash='+genhash)

    for file in file_register.keys():
        if hash_f in file_register[file].values():
            file_hash = file
        else:
            print(hash_f+' is not in ')
            print(file_register[file])
    result = send_file('tmp_parts/'+file_hash+'/' + hash_f + '.part', attachment_filename=hash_f + '.part')
    result.headers["filename"] = hash_f + '.part'
    os.remove('tmp_parts/'+file_hash+'/' + hash_f + '.part')
    return result


app.run()

